// 1. What directive is used by Node.js in loading the modules it needs?

	'Require' directive

// 2. What Node.js module contains a method for server creation?

	The http module in Node.js contains a method called createServer that can be used to create an HTTP server.

// 3. What is the method of the http object responsible for creating a server using Node.js?

	The createServer method of the http object in Node.js is responsible for creating an HTTP server.

// 4. What method of the response object allows us to set status codes and content types?
	
	The writeHead method of the response object in Node.js allows you to set the status code and content type of the response.

// 5. Where will console.log() output its contents when run in Node.js?

	When you run console.log() in Node.js, the output will be printed to the console where you are running the Node.js program.

// 6. What property of the request object contains the address's endpoint?
	
	In Node.js, the url property of the request object contains the endpoint of the address that sent the request.
